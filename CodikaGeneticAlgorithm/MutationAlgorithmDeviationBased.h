//
//  MutationAlgorithmDeviationBased.h
//  CodikaGeneticAlgorithm
//
//  Created by Exequiel Banga on 8/1/18.
//  Copyright © 2018 Codika. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GAManager.h"

@interface MutationAlgorithmDeviationBased : NSObject<MutationAlgorithm>
/**
 *  It mutates the population when deviation < minDeviation
 */
@property(nonatomic,assign)CGFloat deviation;
@property(nonatomic,assign)CGFloat minDeviation;

- (void)updateDeviation:(NSArray *)array;

@end

//
//  NSObject+category.h
//  Compras
//
//  Created by Exequiel Banga on 1/12/17.
//  Copyright © 2015 Codika. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject(NSObject_Category)
- (void) setVoidValue:(void*)value forKey:(NSString *)key;
- (void)collectResultOf:(NSInvocation*)invocation into:(void*)buffer;
- (void)performSelector:(SEL)aSelector afterDelay:(NSTimeInterval)delay returnValueIn:(void*)buffer withParams:(void*)firstParam, ...;
- (void)performSelector:(SEL)aSelector afterDelay:(NSTimeInterval)delay withParams:(void*)firstParam, ...;
- (void*)performSelector:(SEL)aSelector withParams:(void*)firstParam, ...;

@end

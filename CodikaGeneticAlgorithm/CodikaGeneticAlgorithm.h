//
//  CodikaGeneticAlgorithm.h
//  CodikaGeneticAlgorithm
//
//  Created by Exequiel Banga on 7/1/18.
//  Copyright © 2018 Codika. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CodikaGeneticAlgorithm.
FOUNDATION_EXPORT double CodikaGeneticAlgorithmVersionNumber;

//! Project version string for CodikaGeneticAlgorithm.
FOUNDATION_EXPORT const unsigned char CodikaGeneticAlgorithmVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CodikaGeneticAlgorithm/PublicHeader.h>
#import <CodikaGeneticAlgorithm/BinomialCross.h>
#import <CodikaGeneticAlgorithm/GAManager.h>
#import <CodikaGeneticAlgorithm/MutationAlgorithmBasic.h>
#import <CodikaGeneticAlgorithm/TableSelectionAlgorithm.h>
#import <CodikaGeneticAlgorithm/MutationAlgorithmDeviationBased.h>

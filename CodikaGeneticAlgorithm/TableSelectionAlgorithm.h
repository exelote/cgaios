//
//  Selection.h
//  Compras
//
//  Created by Exequiel Banga on 20/12/17.
//  Copyright © 2015 Codika. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAManager.h"

@interface TableSelectionAlgorithm : NSObject<SelectionAlgorithm>

@end

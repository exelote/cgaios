//
//  MutationAlgorithmDeviationBased.m
//  CodikaGeneticAlgorithm
//
//  Created by Exequiel Banga on 8/1/18.
//  Copyright © 2018 Codika. All rights reserved.
//

#import "MutationAlgorithmDeviationBased.h"

@implementation MutationAlgorithmDeviationBased

- (NSNumber *)meanOf:(NSArray *)array
{
    double runningTotal = 0.0;
    
    for(NSNumber *number in array)
    {
        runningTotal += [number doubleValue];
    }
    
    return [NSNumber numberWithDouble:(runningTotal / [array count])];
}

- (NSNumber *)standardDeviationOf:(NSArray *)array
{
    if(![array count]) return nil;
    
    double mean = [[self meanOf:array] doubleValue];
    double sumOfSquaredDifferences = 0.0;
    
    for(NSNumber *number in array)
    {
        double valueOfNumber = [number doubleValue];
        double difference = valueOfNumber - mean;
        sumOfSquaredDifferences += difference * difference;
    }
    
    return [NSNumber numberWithDouble:sqrt(sumOfSquaredDifferences / [array count])];
}

- (void)updateDeviation:(NSArray *)array{
    self.deviation = [[self standardDeviationOf:array] floatValue];
}


- (void)mutate:(NSArray *)population{
    if (self.deviation<self.minDeviation) {
        for (CGFloat i = self.deviation; i < self.minDeviation; i+=0.1) {
            [population[arc4random()%population.count] mutate];
        }
        NSLog(@"Deviation: %f",self.deviation);
        NSLog(@"Mutations: %f",(self.minDeviation-self.deviation)*10.0);
    }
}

@end

//
//  GAManager.m
//  Compras
//
//  Created by Exequiel Banga on 27/12/17.
//  Copyright © 2015 Codika. All rights reserved.
//

#import "GAManager.h"
#import <UIKit/UIKit.h>
#import "MutationAlgorithmDeviationBased.h"

@interface GAManager()
@property(nonatomic,assign)NSUInteger iteration;
@property(nonatomic,assign)CGFloat bestIndividualAptitude;
@property(nonatomic,assign)BOOL shouldStop;

@property(nonatomic,strong)NSObject *bestIndividual;

- (BOOL)shouldFinish;
- (id)bestIndividual;
- (void)selection;
- (void)cross;
- (void)mutation;
@end

@implementation GAManager

#pragma mark - Process
- (void)start{
    self.shouldStop = NO;
    self.iteration = 0;
    self.bestIndividualAptitude = -NSIntegerMax;
    self.bestIndividual = nil;

    while (![self shouldFinish] && !self.shouldStop){
        [self selection];
        [self mutation];
        [self cross];
        self.iteration ++;
        self.finishCicleBlock(self.population,[self bestIndividual]);
    }
    NSLog(@"%i",self.iteration);
    
    if (!self.shouldStop) {
        self.finishBlock(self.population,[self bestIndividual]);
    }
}
- (void)stop{
    self.shouldStop = YES;    
}

- (void)saludar{
    
    printf("Codika les desea: ");
    for (int i = 1; i <= 365; i++) {
        if (i == 256) {
            printf("Feliz día del programador!!!");
        }
        int espera = 60*60*24;
        wait(&espera);
    }
    
}

- (void)selection{
    self.population = [self.selectionAlgorithm selectBestFrom:self.population];
    CGFloat bestPopuationAptitude = [self.selectionAlgorithm.populationAptitudes[0] floatValue];
    NSLog(@"   Best population aptitude: %f",bestPopuationAptitude);
    if (bestPopuationAptitude > self.bestIndividualAptitude) {
        self.bestIndividual = [self.population[0] copy];
        self.bestIndividualAptitude = bestPopuationAptitude;
    }
}

- (void)cross{
    if ([self.crossAlgorithm respondsToSelector:@selector(crossFrom:)]) {
        [self.population addObjectsFromArray:[self.crossAlgorithm crossFrom:self.population]];
    }else{
        NSMutableArray *population = [NSMutableArray arrayWithArray:self.population];
        NSMutableArray *cross = [NSMutableArray new];
        for (NSInteger i = 0; i < population.count; i++) {
            id parent1 = [population firstObject];
            [population removeObjectAtIndex:0];
            i--;
            NSUInteger indexParent2 = arc4random()%population.count;
            id parent2 = [population objectAtIndex:indexParent2];
            [population removeObjectAtIndex:indexParent2];
            i--;
            
            [cross addObjectsFromArray:[self.crossAlgorithm cross:parent1 with:parent2]];
        }
    }
}

- (void)mutation{
    [(MutationAlgorithmDeviationBased *)self.mutationAlgorithm updateDeviation:self.selectionAlgorithm.populationAptitudes];
    [self.mutationAlgorithm mutate:self.population];
}

- (BOOL)shouldFinish{
    return self.iteration >= self.maxNumberOfIterations || self.bestIndividualAptitude >= self.aptitudeThreshold;
}

@end

//
//  NSObject+Category.m
//  Categorias
//
//  Created by Exequiel Banga on 1/12/17.
//  Copyright 2010 Codika. All rights reserved.
//

#import "NSObject+category.h"


@implementation NSObject(NSObject_Category)


#define kMaskSetterRefer @"set%@:"

-(void*)performSelector:(SEL)aSelector withParams:(void*)firstParam, ...{
    va_list argumentList;
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:
                                [self methodSignatureForSelector:aSelector]];
    [invocation setTarget:self];
    [invocation setSelector:aSelector];
    
    if(firstParam){
        [invocation setArgument:&firstParam atIndex:2];
        va_start(argumentList, firstParam);
        void* otherParam;
        for (NSUInteger index = 3;(otherParam = va_arg(argumentList, id)); index++) {
            [invocation setArgument:&otherParam atIndex:index];
            va_end(argumentList);
        }
    }
    
    [invocation invoke];
    
    NSUInteger length = [[invocation methodSignature] methodReturnLength];
    if (length) {
        void *buffer = (void *)malloc(length);
        [invocation getReturnValue:&buffer];
        return buffer;
    }
    return NULL;
}


-(void)performSelector:(SEL)aSelector afterDelay:(NSTimeInterval)delay withParams:(void*)firstParam, ...{
    if(firstParam){
        va_list argumentList;
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:
                                    [self methodSignatureForSelector:aSelector]];
        [invocation setTarget:self];
        [invocation setSelector:aSelector];
        
        
        [invocation setArgument:&firstParam atIndex:2];
        va_start(argumentList, firstParam);
        void* otherParam;
        for (NSUInteger index = 3;(otherParam = va_arg(argumentList, id)); index++) {
            [invocation setArgument:&otherParam atIndex:index];
            va_end(argumentList);
        }
        [invocation performSelector:@selector(invoke) withObject:nil afterDelay:delay];
    }
    else {
        [self performSelector:aSelector withObject:nil afterDelay:delay];
    }
}


-(void)performSelector:(SEL)aSelector afterDelay:(NSTimeInterval)delay returnValueIn:(void*)buffer withParams:(void*)firstParam, ...{
    va_list argumentList;
    NSInvocation *invocation = [[NSInvocation invocationWithMethodSignature:
                                 [self methodSignatureForSelector:aSelector]]retain];
    [invocation setTarget:self];
    [invocation setSelector:aSelector];
    
    if(firstParam){
        [invocation setArgument:&firstParam atIndex:2];
        va_start(argumentList, firstParam);
        void* otherParam;
        for (NSUInteger index = 3;(otherParam = va_arg(argumentList, id)); index++) {
            [invocation setArgument:&otherParam atIndex:index];
            va_end(argumentList);
        }
    }
    [self performSelector:@selector(collectResultOf:into:) afterDelay:delay withParams:invocation,buffer,nil];
}

-(void)collectResultOf:(NSInvocation*)invocation into:(void*)buffer{
    NSUInteger length = [[invocation methodSignature] methodReturnLength];
    [invocation invoke];
    if (length)
        [invocation getReturnValue:buffer];
    [invocation release];
}

- (void) setVoidValue:(void*)value forKey:(NSString *)key {
    NSString *capitalisedSentence = [[NSString alloc] initWithString:[NSString stringWithFormat:kMaskSetterRefer,[key stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[key substringToIndex:1] capitalizedString]]]];
    if ([self respondsToSelector:sel_registerName([capitalisedSentence cStringUsingEncoding:NSISOLatin1StringEncoding])]) {
        [self performSelector:sel_registerName([capitalisedSentence cStringUsingEncoding:NSISOLatin1StringEncoding]) withObject:value];        
    }
    [capitalisedSentence release];
}


@end

//
//  ViewController.m
//  Genetic Algorithm
//
//  Created by Exequiel Banga on 28/3/17.
//  Copyright © 2017 Codika. All rights reserved.
//

#import "ViewController.h"

#import "PlayingStyle.h"
#import <CodikaGeneticAlgorithm/GAManager.h>
#import <CodikaGeneticAlgorithm/TableSelectionAlgorithm.h>
#import <CodikaGeneticAlgorithm/BinomialCross.h>
#import <CodikaGeneticAlgorithm/MutationAlgorithmBasic.h>

@interface ViewController ()
@end

@implementation ViewController

- (void)buscarSolucion{
    GAManager *gaManager = [GAManager new];
    gaManager.maxNumberOfIterations = 100;
    gaManager.aptitudeThreshold = 1000000;
    gaManager.population = [NSMutableArray new];
    for (NSInteger i = 0; i < 40; i++) {
        PlayingStyle *solucion = [PlayingStyle new];
        
        [gaManager.population addObject:solucion];
    }
    
    gaManager.selectionAlgorithm = [TableSelectionAlgorithm new];
    [gaManager.selectionAlgorithm setGenerationalJump:@(.5)];
    [gaManager.selectionAlgorithm setAptitudeBlock:[PlayingStyle aptitudeBlock]];
    
    gaManager.crossAlgorithm = [BinomialCross new];
    
    gaManager.mutationAlgorithm = [MutationAlgorithmBasic new];
    [(MutationAlgorithmBasic *)gaManager.mutationAlgorithm setInverseProbability:100];
    
    gaManager.finishBlock = ^(NSArray *finalPopulation,PlayingStyle *bestIndividual){
        NSLog(@"Best individual's money: %ld",[bestIndividual money]);
        NSLog(@"Family tree result: %f",[bestIndividual evaluateFamilyTree]);
        
    };
    [gaManager start];
}

#pragma mark - viewDidload
- (void)viewDidLoad{
    [self buscarSolucion];
}

@end

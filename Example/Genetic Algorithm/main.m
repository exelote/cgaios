//
//  main.m
//  Genetic Algorithm
//
//  Created by Exequiel Banga on 28/3/17.
//  Copyright © 2017 Codika. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

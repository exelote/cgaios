//
//  ViewController.h
//  Genetic Algorithm
//
//  Created by Exequiel Banga on 28/3/17.
//  Copyright © 2017 Codika. All rights reserved.
//
/**
Lets assume you need to go as many shops you can, but only walking n blocks.
 This example will receive a set of geographic points, one per shop, and will generate an ordered sub set with a possible conbination that allows you to visit a good number of shops, walking n blocks at maximum.
 
 */

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController


@end


//
//  AppDelegate.h
//  Genetic Algorithm
//
//  Created by Exequiel Banga on 28/3/17.
//  Copyright © 2017 Codika. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


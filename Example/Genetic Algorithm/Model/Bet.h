//
//  Bet.h
//  Compras
//
//  Created by Exequiel Banga on 1/1/18.
//  Copyright © 2018 Codika. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Bet : NSObject

@property (nonatomic, strong)NSMutableDictionary *numbers;
@property (nonatomic, strong)NSMutableDictionary *dozens;
@property (nonatomic, strong)NSMutableDictionary *columns;
@property (nonatomic, assign)NSInteger odd;
@property (nonatomic, assign)NSInteger even;
@property (nonatomic, assign)NSInteger red;
@property (nonatomic, assign)NSInteger black;

@property (nonatomic, assign)CGFloat riskLevel;

+ (instancetype)randomBetWithMoney:(NSInteger)money;

@end

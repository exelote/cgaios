//
//  PlayingStyle.m
//  Compras
//
//  Created by Exequiel Banga on 1/1/18.
//  Copyright © 2018 Codika. All rights reserved.
//

#import "PlayingStyle.h"
#import <CodikaGeneticAlgorithm/BinomialCross.h>

@implementation PlayingStyle

+ (BOOL)isRed:(NSInteger)number{
    return YES;
}

+ (BOOL)isBlack:(NSInteger)number{
    return ![self isRed:number]&& number != 0;
}

+ (AptitudeBlock)aptitudeBlock{
    return ^(PlayingStyle *playingStile){
//        playingStile.money = 1000;
        for (NSInteger i=0; i<1; i++) {
            NSInteger score = 0;
            NSInteger number = arc4random()%37;
            
            score += 36 * [playingStile.bet.numbers[@(number)] integerValue];
            score += 3*[playingStile.bet.dozens[@(1 + number/12)] integerValue];
            score += 3*[playingStile.bet.columns[@(number%3==0?3:number%3)] integerValue];
            
            
            if (number%2 == 0) {
                score += 2*playingStile.bet.even;
            }else{
                score += 3*playingStile.bet.odd;
            }
            
            if ([self isRed:number]) {
                score += 2*playingStile.bet.red;
            }else if([self isBlack:number]){
                score += 2*playingStile.bet.black;
            }
            
            playingStile.money += score;
            
            for (NSInteger i = 0; i <=36; i++) {
                playingStile.money -= [playingStile.bet.numbers[@(i)] integerValue];
            }
            playingStile.money -= [playingStile.bet.dozens[@(1)]integerValue];
            playingStile.money -= [playingStile.bet.dozens[@(2)]integerValue];
            playingStile.money -= [playingStile.bet.dozens[@(3)]integerValue];

            playingStile.money -= [playingStile.bet.columns[@(1)]integerValue];
            playingStile.money -= [playingStile.bet.columns[@(2)]integerValue];
            playingStile.money -= [playingStile.bet.columns[@(3)]integerValue];

            playingStile.money -= playingStile.bet.even;
            playingStile.money -= playingStile.bet.odd;
            playingStile.money -= playingStile.bet.red;
            playingStile.money -= playingStile.bet.black;
        }
        
        return (CGFloat)playingStile.money;
    };
}

- (CGFloat)evaluateFamilyTree{
    NSMutableArray *tree = [NSMutableArray new];
    
    PlayingStyle *playingStyle = self;
    while (playingStyle.father) {
        [tree insertObject:playingStyle atIndex:0];
        playingStyle = playingStyle.father;
    }
    
    CGFloat money = 100;
    for (PlayingStyle *playingStyle in tree) {
        playingStyle.money = money;
        money   = [[self class] aptitudeBlock](playingStyle);
    }
    NSLog(@"Ancestors: %lu", tree.count-1);
    return money;
}

+ (NSArray *)cross:(PlayingStyle *)individualA with:(PlayingStyle *)individualB{
    NSArray *children = [BinomialCross crossPropertiesOf:individualA withPropertiesOf:individualB];
    if ([children[0] money] == individualA.money) {
        [children[0] setFather:individualA];
    }else{
        [children[0] setFather:individualB];
    }

    if ([children[1] money] == individualA.money) {
        [children[1] setFather:individualA];
    }else{
        [children[1] setFather:individualB];
    }

    return children;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        self.money = 100;
        self.bet = [Bet randomBetWithMoney:self.money];
    }
    return self;
}

- (void)mutate{
    self.bet = [Bet randomBetWithMoney:1000];
}

@end

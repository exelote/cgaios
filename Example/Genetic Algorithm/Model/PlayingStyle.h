//
//  PlayingStyle.h
//  Compras
//
//  Created by Exequiel Banga on 1/1/18.
//  Copyright © 2018 Codika. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CodikaGeneticAlgorithm/GAManager.h>
#import "Bet.h"

@interface PlayingStyle : NSObject<Chromosome>

@property(nonatomic,strong)Bet *bet;
@property(nonatomic,assign)NSInteger money;

@property(nonatomic,strong)PlayingStyle  *father;

+ (AptitudeBlock)aptitudeBlock;

- (CGFloat)evaluateFamilyTree;

@end

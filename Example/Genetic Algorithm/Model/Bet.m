//
//  Bet.m
//  Compras
//
//  Created by Exequiel Banga on 1/1/18.
//  Copyright © 2018 Codika. All rights reserved.
//

#import "Bet.h"

@implementation Bet

-(instancetype)init{
    self = [super init];
    if (self) {
        self.numbers = [NSMutableDictionary new];
        for (NSInteger i = 0; i <=36; i++) {
            self.numbers[@(i)] = @(0);
        }
        
        self.dozens = [NSMutableDictionary new];
        self.dozens[@(1)] = @(0);
        self.dozens[@(2)] = @(0);
        self.dozens[@(3)] = @(0);
        
        self.columns = [NSMutableDictionary new];
        self.columns[@(1)] = @(0);
        self.columns[@(2)] = @(0);
        self.columns[@(3)] = @(0);
        
        self.even = 0;
        self.odd = 0;
        self.red = 0;
        self.black = 0;
    }
    return self;
}

+ (instancetype)randomBetWithMoney:(NSInteger)money{
    Bet *bet = [Bet new];
    bet.riskLevel = (arc4random()%100)/100.f;
    NSInteger amountToUse = money * bet.riskLevel;
    NSInteger betProbability = 10;

    while (amountToUse >0) {
        
        for (NSInteger i = 0; i <=36; i++) {
            if (amountToUse > 0 && arc4random()%betProbability) {
                bet.numbers[@(i)] = @([bet.numbers[@(i)] integerValue]+1);
                amountToUse--;
            }
        }
        if (amountToUse > 0) {
            for (NSInteger i = 1; i <=3; i++) {
                if (amountToUse > 0 && arc4random()%betProbability) {
                    bet.dozens[@(i)] = @([bet.dozens[@(i)] integerValue]+1);
                    amountToUse--;
                }
            }
        }
        
        if (amountToUse > 0) {
            for (NSInteger i = 1; i <=3; i++) {
                if (arc4random()%betProbability) {
                    bet.columns[@(i)] = @([bet.columns[@(i)] integerValue]+1);
                    amountToUse--;
                }
            }
        }
        
        if (amountToUse > 0 && arc4random()%betProbability) {
            bet.even += 1;
            amountToUse--;
        }
        if (amountToUse > 0 && arc4random()%betProbability) {
            bet.odd += 1;
            amountToUse--;
        }
        if (amountToUse > 0 && arc4random()%betProbability) {
            bet.red += 1;
            amountToUse--;
        }
        if (amountToUse > 0 && arc4random()%betProbability) {
            bet.black += 1;
            amountToUse--;
        }
    }
    
    return bet;
}

@end
